from flask import Flask, Response
from flask import request
import pymongo
import dns
import json
from bson.objectid import ObjectId
app=Flask(__name__)


try:
    client = pymongo.MongoClient("mongodb+srv://Nishchay:nishchay@cluster0.pfklt.mongodb.net/test?retryWrites=true&w=majority")
    db = client.get_database('Company')
    client.server_info()

        # mongo=pymongo.MongoClient(
        #     host="localhost",
        #     port=27017,
        #     serverSelectionTimeoutMS=1000
        # )
        # db=mongo.company
        # mongo.server_info()
       

except:
    print("Could Not connect to the database")




@app.route("/users",methods=["GET"])
def get_user():
    try:
        data=list(db.user.find())
        for users in data:
            users['_id']=str(users['_id'])

        print(data)

        return Response(
            response=json.dumps(data),
            status=200,
            mimetype="application/json"
        )
        
    except Exception as ex:
        print(ex)
        return Response(
            response=json.dumps({"message":"Cannot find users"}),
            status=500,
            mimetype="application/json"
        )





@app.route("/users",methods=["POST"])
def create_user():
    try:
        user={
            "firstName":request.form['firstName'],
            "lastName":request.form["lastName"]
           
        }
        print(user)
        dbResponse=db.user.insert_one(user)   
        return Response(
            response=json.dumps({"message":"User created","id":f"{dbResponse.inserted_id}"}),
            status=200,
            mimetype="application/json"
        )
    except Exception as ex:
        print(ex)
        



@app.route("/users/<id>",methods=["PATCH"])
def update_user(id):
     try:
        dbResponse=db.user.update_one(
            {"_id":ObjectId(id)},
            {"$set":{"firstName":request.form["firstName"],"lastName":request.form["lastName"]}}
        )
        return Response(
            response=json.dumps({"message":"User updated"}),
            status=200,
            mimetype="application/json"
        )
        
     except Exception as ex:
        print(ex)
        return Response(
            response=json.dumps({"message":"Cannot update the user"}),
            status=500,
            mimetype="application/json"
        )



@app.route("/users/<id>",methods=["DELETE"])
def delete_user(id):
     try:
        dbResponse=db.user.delete_one(
            {"_id":ObjectId(id)}
        )
        return Response(
            response=json.dumps({"message":"User deleted","id":f"{id}"}),
            status=200,
            mimetype="application/json"
        )
        
     except Exception as ex:
        print(ex)
        return Response(
            response=json.dumps({"message":"Cannot delete the user"}),
            status=500,
            mimetype="application/json"
        )


        

@app.route("/users/<id>",methods=["GET"])
def get_user_byId(id):
     try:
        dbResponse=db.user.find_one(
            {"_id":ObjectId(id)}
        )
        print(dbResponse)
        return Response(
            response=json.dumps({"id":f"{id}","firstName":dbResponse["firstName"],"lastName":dbResponse["lastName"]}),
            status=200,
            mimetype="application/json"
        )
        
     except Exception as ex:
        print(ex)
        return Response(
            response=json.dumps({"message":"User not found"}),
            status=500,
            mimetype="application/json"
        )


if __name__== "__main__":
    app.run(port=80,debug=True)
